# wkd-helper

Commandline helpers for handling OpenPGP Web Key Directories (WKD)

## License

(c) 2022 by ulif <uli@gnufix.de>
Licenced under AGPL-3.0-or-later

src/zbase32.py is (c) Daniel Knell <contact@danielknell.co.uk>
Licenced under [MIT licence](http://dan.mit-license.org/).

All documentation and images are licenced under the [Creative Commons
Attribution-ShareAlike 4.0 International License][cc_by_sa].

[cc_by_sa]: https://creativecommons.org/licenses/by-sa/4.0/

