import argparse
import hashlib
import json
import requests
import sys
from wkd_helper import zbase32


__version__ = "0.0.1"


GPL_TEXT = """
     This program is free software: you can redistribute it and/or modify it
     under the terms of the GNU Affero General Public License as published by
     the Free Software Foundation, either version 3 of the License, or (at your
     option) any later version.

     This program is distributed in the hope that it will be useful, but
     WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
     or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
     License for more details.

     You should have received a copy of the GNU Affero General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.
    """


def print_hash_version():
    """Output current version and other infos."""
    print("wkd-hash %s" % __version__)
    print("Copyright (C) 2022 ulif")
    print(GPL_TEXT)


def handle_hash_options(args):
    """Handle commandline arguments."""
    parser = argparse.ArgumentParser(
        description=(
            "Turn string into wkd hash as used by WKD/WKS keyservers."
        )
    )
    g = parser.add_mutually_exclusive_group(required=True)
    g.add_argument(
        "--version",
        action="store_true",
        required=False,
        help="output version information and exit.",
    )
    g.add_argument(
        "email", metavar="EMAIL", nargs="?", default=None,
        help="An email-address"
    )
    args = parser.parse_args(args)
    return args


def wkd_hash(email):
    user = email.lower().split("@")[0]
    result = zbase32.encode(hashlib.sha1(user.encode("utf-8")).digest())
    return result


def wkd_urls(email):
    email = email.lower()
    hashed = wkd_hash(email)
    user, domain = email.split("@", 1)
    direct = "https://%s/.well-known/openpgpkey/hu/%s?l=%s" % (domain, hashed, user)
    direct_policy = "https://%s/.well-known/openpgpkey/policy" % (domain)
    advanced = "https://openpgpkey.%s/.well-known/openpgpkey/%s/hu/%s?l=%s" % (domain, domain, hashed, user)
    advanced_policy = "https://openpgpkey.%s/.well-known/openpgpkey/%s/policy" % (domain, domain)
    return dict(
            direct=direct, direct_policy=direct_policy, advanced=advanced,
            advanced_policy=advanced_policy
    )


def main_hash(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    args = handle_hash_options(argv)
    if args.version:
        print_hash_version()
        sys.exit(0)
    print(wkd_hash(args.email))


def main_urls(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    args = handle_hash_options(argv)
    if args.version:
        print_hash_version()
        sys.exit(0)
    print(json.dumps(wkd_urls(args.email), indent=2))
